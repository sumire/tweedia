package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/docker/go-units"
	"github.com/fatih/color"
	_ "github.com/mattn/go-sqlite3"
	twitterscraper "github.com/n0madic/twitter-scraper"
	"github.com/schollz/progressbar/v3"
	"github.com/spf13/pflag"
	"golang.org/x/sys/unix"
)

// Requests
type Download struct {
	URL  string
	Date time.Time
}

// count
var (
	tt int
	it int
	vt int
)

// Colors
var (
	succ  = color.New(color.FgHiGreen, color.Bold).PrintlnFunc()
	info  = color.New(color.FgHiBlue, color.Bold).PrintlnFunc()
	warn  = color.New(color.FgHiYellow, color.Bold).PrintlnFunc()
	fatal = color.New(color.FgHiRed, color.Bold).PrintlnFunc()
)

func main() {
	// Cli args
	var (
		dir      string
		username string
		keyword  string
		hashtag  string
		since    string
		until    string
		photo    bool
		videos   bool
		cursor   string
		status   int64
		minsize  string
		maxsize  string
		database string
	)

	// Init
	scraper := twitterscraper.New()

	wg := &sync.WaitGroup{}
	defer wg.Wait()

	// Parse cli flags
	pflag.StringVarP(&dir, "dir", "d", "", "Set output directory")
	pflag.StringVarP(&username, "username", "n", "", "Filter by username")
	pflag.StringVarP(&keyword, "keyword", "k", "", "Search keyword")
	pflag.StringVarP(&hashtag, "hashtag", "t", "", "Search by hashtag")
	pflag.StringVarP(&since, "since", "s", "", "Filter by since date")
	pflag.StringVarP(&until, "until", "u", "", "Filter by until date")
	pflag.BoolVarP(&photo, "photo", "p", false, "Filter with photos only")
	pflag.BoolVarP(&videos, "video", "v", false, "Filter with videos only")
	pflag.StringVarP(&cursor, "cursor", "c", "", "Continue from cursor")
	pflag.Int64VarP(&status, "status", "i", 0, "Set max tweet status id")
	pflag.StringVarP(&minsize, "min", "m", "", "Set min size limit")
	pflag.StringVarP(&maxsize, "max", "x", "", "Set max size limit")
	pflag.StringVarP(&database, "database", "f", "", "Set database file path")
	pflag.Parse()

	// Check args
	var err error
	dateOnly := "2006-01-02"
	layout := "01/02/2006"

	if len(os.Args) == 1 {
		fmt.Printf("Usage of %s:\n", filepath.Base(os.Args[0]))
		pflag.PrintDefaults()
		os.Exit(0)
	}

	// Build search query
	var query string
	var binds []interface{}
	msql := "SELECT * FROM tweets"

	if len(hashtag) > 0 {
		query = "#" + hashtag
		msql += " AND WHERE hashtags LIKE ?"
		binds = append(binds, "%"+hashtag+"%")
	}

	if len(keyword) > 0 {
		query += keyword

		// Use keyword as hashtag
		if strings.HasPrefix(keyword, "#") {
			if len(hashtag) > 0 {
				msql += " OR WHERE hashtags LIKE ?"
			} else {
				msql += " AND WHERE hashtags LIKE ?"
			}
		} else {
			msql += " AND WHERE content LIKE ?"
		}
		binds = append(binds, "%"+keyword+"%")
	}

	if len(username) > 0 {
		query += " from:@" + username
		msql += " AND WHERE username = ?"
		binds = append(binds, username)
	}

	if len(hashtag) == 0 && len(keyword) == 0 && len(username) == 0 {
		fatal("Username, keyword or hashtag must be set.")
		os.Exit(1)
	}

	// Check date
	var sd time.Time
	if len(since) > 0 {
		sd, err := time.Parse(dateOnly, since)
		if err != nil {
			sd, err = time.Parse(layout, since)
			if err != nil {
				fatal("Invalid since date " + since)
				os.Exit(1)
			}
		}

		since = sd.Format(dateOnly)
		query += " since:" + since
	}

	if len(until) > 0 {
		ud, err := time.Parse(dateOnly, until)
		if err != nil {
			ud, err = time.Parse(layout, until)
			if err != nil {
				fatal("Invalid until date " + until)
				os.Exit(1)
			}
		}

		until = ud.Format(dateOnly)
		query += " until:" + until

		if sd.After(ud) || since == until {
			fatal(fmt.Sprintf("Since date %s cannot be same or later than until date %s", since, until))
			os.Exit(1)
		}
	}

	switch {
	case len(since) > 0 && len(until) > 0:
		msql += " AND published_at BETWEEN ? AND ?"
		binds = append(binds, since, until)
	case len(since) > 0:
		msql += " AND published_at >= ?"
		binds = append(binds, since)
	case len(until) > 0:
		msql += " AND published_at <= ?"
		binds = append(binds, until)
	}

	// Set max tweet status id in sql query
	if status > 0 {
		msql += " AND status <= ?"
		binds = append(binds, status)
	}

	// Check min and max size
	var min, max int64
	if len(minsize) > 0 {
		min, err = units.FromHumanSize(minsize)
		if err != nil {
			fatal("Invalid min size " + minsize)
			os.Exit(1)
		}
	}

	if len(maxsize) > 0 {
		max, err = units.FromHumanSize(maxsize)
		if err != nil {
			fatal("Invalid max size " + maxsize)
			os.Exit(1)
		}
	}

	if min > 0 && max > 0 && min > max {
		fatal(fmt.Sprintf("Media max size %d must be greater than min size %d", max, min))
		os.Exit(1)
	}

	// Check media filters
	if photo {
		query += " filter:images"
	}

	if videos {
		query += " filter:native_video"
	}

	// Fix sql query
	msql += " ORDER BY status DESC"
	msql = strings.Replace(msql, " AND WHERE ", " WHERE ", 1)

	// Add prefix to cursor
	if len(cursor) > 0 && !strings.HasPrefix(cursor, "scroll:") {
		cursor = "scroll:" + cursor
	}

	// Create directory if not exists
	if len(dir) > 0 {
		d, err := os.Open(dir)
		if err != nil {
			if err := os.MkdirAll(dir, 0750); err != nil {
				fatal("Failed to create directory: " + err.Error())
				os.Exit(1)
			}
		} else {
			fi, _ := d.Stat()
			if !fi.IsDir() {
				fatal(fmt.Sprintf("%s is a file.", dir))
				os.Exit(1)
			}
		}
	}

	// Database
	if len(database) > 0 {
		// Check if database file is exists. otherwise ignore it
		fi, err := os.Stat(database)
		if errors.Is(err, fs.ErrNotExist) || fi.IsDir() {
			warn(fmt.Sprintf("Database file %s does not exists or is a directory, fallback to default settings.", database))
			database = "tweets.db"
		}
	} else {
		database = "tweets.db"
	}

	db, err := sql.Open("sqlite3", database)
	if err != nil {
		fatal("Cannot open Database: " + err.Error())
		os.Exit(1)
	}
	defer db.Close()

	tb := `CREATE TABLE IF NOT EXISTS tweets(
			status INTEGER PRIMARY KEY UNIQUE,
			username TEXT NOT NULL,
			nickname TEXT NOT NULL,
			link TEXT NOT NULL,
			content TEXT NOT NULL,
			raw TEXT NOT NULL,
			hashtags TEXT,
			media TEXT,
			published_at DATETIME NOT NULL,
			likes INTEGER DEFAULT 0,
			replies INTEGER DEFAULT 0,
			retweets INTEGER DEFAULT 0,
			is_quoted INTEGER,
			is_reply INTEGER,
			is_retweet INTEGER
            )`
	tb2 := `CREATE TABLE IF NOT EXISTS users(
			id INTEGER PRIMARY KEY,
			username TEXT NOT NULL UNIQUE,
			nickname TEXT NOT NULL,
			bio TEXT,
			website TEXT,
			location TEXT,
			birthday TEXT,
			registered_at DATETIME,
			link TEXT NOT NULL,
			avatar TEXT,
			banner TEXT,
			tweets INTEGER DEFAULT 0,
			likes INTEGER DEFAULT 0,
			listed INTEGER DEFAULT 0,
			followers INTEGER DEFAULT 0,
			following INTEGER DEFAULT 0,
			friends INTEGER DEFAULT 0
			)`
	if _, err := db.Exec(tb); err != nil {
		fatal("SQL error: " + err.Error())
		fmt.Println("SQL query: " + tb)
		os.Exit(1)
	}
	if _, err := db.Exec(tb2); err != nil {
		fatal("SQL error: " + err.Error())
		fmt.Println("SQL query: " + tb2)
		os.Exit(1)
	}

	// Set proxies
	proxies := []string{"HTTPS_PROXY", "https_proxy", "HTTP_PROXY", "http_proxy", "ALL_PROXY", "all_proxy"}
	for _, proxy := range proxies {
		if len(os.Getenv(proxy)) > 0 {
			if err := scraper.SetProxy(os.Getenv(proxy)); err == nil {
				break
			}
		}
	}

	scraper.SetSearchMode(twitterscraper.SearchLatest)

	// Logging
	f, err := os.OpenFile("scraper.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fatal("logging error: " + err.Error())
		os.Exit(1)
	}
	defer f.Close()
	log.SetOutput(f)

	// Handle interrupt signal (Ctrl+C)
	ctx, cancel := context.WithCancel(context.Background())
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill, syscall.SIGTERM)

	// Start
	info("Fetching tweets ...")
	log.Println("Search query: " + query)

	if len(username) > 0 && len(query) == 0 {
		// Get user profile
		pro, err := scraper.GetProfile(username)
		if err != nil {
			fatal("User does not exist: " + err.Error())
			os.Exit(1)
		}

		info("Fetching user %s (@%s) %d tweets ...\n", pro.Name, pro.Username, pro.TweetsCount)
		info("-------------------------")

		// Save to database
		sql := "INSERT OR IGNORE INTO users (id, username, nickname, bio, website, location, birthday, registered_at, link, avatar, banner, tweets, likes, listed, followers, following, friends) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
		if _, err := db.Exec(sql, pro.UserID, pro.Username, pro.Name, pro.Biography, pro.Website, pro.Location, pro.Birthday, pro.Joined, pro.URL, pro.Avatar, pro.Banner, pro.TweetsCount, pro.LikesCount, pro.ListedCount, pro.FollowersCount, pro.FollowingCount, pro.FriendsCount); err != nil {
			fatal("SQL error: " + err.Error())
			fmt.Println(sql)
			log.Println("[ERR} DB: " + err.Error())
			os.Exit(1)
		}
	}

	go func() {
		select {
		case <-ctx.Done():
			return
		default:
			// Search tweets from twitter
			if len(dir) == 0 {
				var tweets []*twitterscraper.Tweet
				var next string
				var tried int

				// Try to fetch next page when no more tweets (may be a bug)
				for tried <= 3 {
					if len(username) > 0 {
						tweets, next, err = scraper.WithDelay(1).FetchTweets(username, 100000, cursor)
					} else {
						tweets, next, err = scraper.WithDelay(1).FetchSearchTweets(query, 100000, cursor)
					}

					if err != nil {
						warn("Fetch tweets error: " + err.Error())
						log.Println("[WARN] NET: " + err.Error())

						continue
					}

					if len(tweets) > 0 {
						// Reset tried count
						tried = 0
						tt += len(tweets)
						succ(fmt.Sprintf("Fetched %d tweets.", len(tweets)))
					} else {
						// End
						if len(cursor) == 0 && tried == 0 {
							// Extract search keyword from query
							s := strings.Split(query, " ")
							warn(fmt.Sprintf("Search %s does not exists.\n", s[0]))
							cancel()

							break
						}

						if tried >= 3 {
							warn("No more tweets.")
							cancel()

							break
						} else {
							tried++
						}
					}

					for i, tweet := range tweets {
						media := []string{}

						// Output tweets
						if i == 0 {
							info("-------------------------")
							fmt.Printf("The first tweet %s published at %s\n", tweet.ID, tweet.TimeParsed)
						}

						// Get media
						if len(tweet.Photos) > 0 {
							media = tweet.Photos
						}

						if len(tweet.Videos) > 0 {
							for _, video := range tweet.Videos {
								media = append(media, video.URL)
							}
						}

						// Save to database
						sql := "INSERT OR IGNORE INTO tweets (status, username, nickname, link, content, raw, hashtags, media, published_at, likes, replies, retweets, is_quoted, is_reply, is_retweet) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
						if _, err := db.Exec(sql, tweet.ID, tweet.Username, "", tweet.PermanentURL, tweet.Text, tweet.HTML, strings.Join(tweet.Hashtags, "\n"), strings.Join(media, "\n"), tweet.TimeParsed, tweet.Likes, tweet.Replies, tweet.Retweets, tweet.IsQuoted, tweet.IsReply, tweet.IsRetweet); err != nil {
							fatal("SQL error: " + err.Error())
							fmt.Println("SQL query: " + sql)
							log.Fatalln("[ERR] DB: " + err.Error())
						}
					}

					cursor = next
					if tried == 0 {
						info("-------------------------")
						info("Fetch more tweets ...")
					}
				}
			} else {
				// Search tweets from database
				var tweet twitterscraper.Tweet
				rows, err := db.Query(msql, binds...)
				if err != nil {
					fatal("SQL error: " + err.Error())
					fmt.Println("SQL query: " + msql)
					log.Fatalln("[ERR] DB: " + err.Error())
				}
				defer rows.Close()

				for rows.Next() {
					var name, tags, media string

					if err := rows.Scan(&tweet.ID, &tweet.Username, &name, &tweet.PermanentURL, &tweet.Text, &tweet.HTML, &tags, &media, &tweet.TimeParsed, &tweet.Likes, &tweet.Replies, &tweet.Retweets, &tweet.IsQuoted, &tweet.IsReply, &tweet.IsRetweet); err != nil {
						fatal("SQL error: " + err.Error())
						log.Fatalln("[ERR] DB: " + err.Error())
					}

					if rows.Err() != nil {
						warn("Database rows error: " + err.Error())
						log.Println(rows.Err())

						continue
					}

					// Output tweets
					info("-------------------------")
					fmt.Printf("Get tweet %s by (@%s), published at %s\n", tweet.ID, tweet.Username, tweet.TimeParsed)

					tt++

					// Download media
					var dls []Download
					urls := strings.Split(media, "\n")

					info(fmt.Sprintf("Found %d media.\n", len(urls)))

					for _, url := range urls {
						dl := Download{
							URL:  url,
							Date: tweet.TimeParsed,
						}
						dls = append(dls, dl)
					}

					// Check disk free spac
					var fs unix.Statfs_t
					unix.Statfs(dir, &fs)
					free := fs.Bfree * uint64(fs.Bsize)
					if free <= 10240 {
						fatal("Your disk is full, please cleanup to free up space")
						os.Exit(1)
					}

					wg.Add(len(dls))

					if err := download(dir, dls, min, max, wg); err != nil {
						// Ignore before download check errors
						if strings.Contains(err.Error(), "Failed") {
							warn("Download error: " + err.Error())
							log.Println("[ERR] DL: " + err.Error())
						}
					}
				}
			}
		}

		// Finished
		cleanup()
	}()

	defer cleanup()

	// Cancelled
	<-c
	warn("Cancelled by user, quitting ...")
	if len(cursor) > 0 {
		log.Println("[WARN] STOP: " + cursor)
	}
	cancel()
}

func download(path string, dls []Download, min, max int64, wg *sync.WaitGroup) error {
	const USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"
	client := &http.Client{}

	defer wg.Done()

	// Show a total download progress bar
	tb := progressbar.NewOptions(
		len(dls),
		//progressbar.OptionSetWriter(ansi.NewAnsiStdout()),
		progressbar.OptionEnableColorCodes(true),
		progressbar.OptionShowBytes(true),
		//progressbar.OptionSetWidth(15),
		progressbar.OptionSetDescription("[cyan][reset] Downloading tweets media ..."),
		progressbar.OptionSetTheme(progressbar.Theme{
			Saucer:        "[green]=[reset]",
			SaucerHead:    "[green]>[reset]",
			SaucerPadding: " ",
			BarStart:      "[",
			BarEnd:        "]",
		}),
	)

	for _, dl := range dls {
		// Create a http request
		req, err := http.NewRequest("GET", dl.URL+"?name=orig", nil)
		if err != nil {
			return err
		}

		// Get the filename from clean url
		url := dl.URL
		s := strings.Split(url, "?")
		url = s[0]
		fn := filepath.Base(dl.URL)
		file := path + "/" + fn

		// Check if file is exists or zero size
		fi, err := os.Stat(file)
		if errors.Is(err, fs.ErrNotExist) || fi.Size() == 0 {
			resp, err := client.Do(req)
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			// limit file size
			fs := units.HumanSize(float64(resp.ContentLength))
			ms := units.HumanSize(float64(min))
			xs := units.HumanSize(float64(max))

			if resp.ContentLength > 0 {
				if min > 0 && resp.ContentLength < min {
					warn(fmt.Sprintf("Skipped file %s because the size %s is smail than %s.\n", fn, fs, ms))
					continue
				}
				if max > 0 && resp.ContentLength > max {
					warn(fmt.Sprintf("Skipped file %s because the size %s is large than %s.\n", fn, fs, xs))
					continue
				}
			}

			// Create new file
			out, err := os.Create(file)
			if err != nil {
				return err
			}

			// Try to download file
			var tried int
			for tried <= 3 {
				// Show a progress bar for each download thread
				pb := progressbar.DefaultBytes(
					resp.ContentLength,
					fmt.Sprintf("Downloading file %s ...\n", fn),
				)
				tb.Add(1)

				// Add filename to error message
				if _, err := io.Copy(io.MultiWriter(out, pb), resp.Body); err != nil {
					if tried >= 3 {
						err = fmt.Errorf("Failed to download file %s: %s\n", fn, err.Error())
						return err
					}
				}

				tried++
			}

			// Modify file metadata
			info(fmt.Sprintf("Setting file %s date to %s ...\n", fn, dl.Date))

			// Update count
			if filepath.Ext(file) == ".jpg" || filepath.Ext(file) == ".png" {
				it++
			} else if filepath.Ext(file) == ".mp4" {
				vt++
			}
		} else {
			warn("Skipped exists file " + fn)
			// Modify file metadata
			info(fmt.Sprintf("Setting exists file %s date to %s ...\n", fn, dl.Date))
		}

		os.Chtimes(file, dl.Date, dl.Date)
	}

	return nil
}

func cleanup() {
	if tt > 0 {
		succ("All done.")
	}

	var msg string
	if it > 0 || vt > 0 {
		msg = fmt.Sprintf("Fetched %d tweets, downloaded %d photo and %d videos.", tt, it, vt)
	} else {
		msg = fmt.Sprintf("Fetched %d tweets.", tt)
	}

	succ(msg)
	log.Println("[SUCC]: " + msg)

	os.Exit(0)
}
