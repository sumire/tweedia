module codeberg.org/sumire/tweedia

go 1.17

require (
	github.com/docker/go-units v0.5.0
	github.com/fatih/color v1.13.0
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/n0madic/twitter-scraper v0.0.0-20230112093343-9231dc0708ee
	github.com/schollz/progressbar/v3 v3.13.0
	github.com/spf13/pflag v1.0.5
	golang.org/x/sys v0.4.0
)

require (
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect
	github.com/rivo/uniseg v0.4.3 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/term v0.4.0 // indirect
)
